import django
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('champion.urls')),
    path('', include('userextend.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('contact.urls')),
]
