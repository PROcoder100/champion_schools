from django.core.mail import send_mail
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from contact.forms import ContactForm
from contact.models import Contact


class ContactView(CreateView):
    template_name = 'contact/contact.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('homepage')


def contact_us(request):
    if request.method == 'POST':
        subject = request.POST['txtSubject']
        message = request.POST['txtMsg']

        send_mail(
            subject,
            message,
            ['elena.alina1411@gmail.com'] # To email - se pot adauda mai multe adrese
        )

        return render(request, 'contact/contact.html')
    else:
        return render(request, 'contact/contact.html')
