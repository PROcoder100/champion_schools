from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import TextInput


class UserExtendForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username']
        widgets = {
            'first_name': TextInput(
                attrs={'placeholder': 'Te rog să îți introduci prenumele', 'class': 'form-control'}),
            'last_name': TextInput(
                attrs={'placeholder': 'Te rog să îți introduci numele de familie', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Te rog să introduci adresa de email', 'class': 'form-control'}),
            'username': TextInput(
                attrs={'placeholder': 'Te rog să introduci numele de utilizator', 'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(UserExtendForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Te rog să îți introduci parola'
        self.fields['password2'].widget.attrs['placeholder'] = 'Te rog să confirmi parola introdusă'
