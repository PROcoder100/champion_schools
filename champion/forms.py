from django import forms
from django.forms import TextInput, Select, DateInput

from champion.models import School, Student, Action, Sport, Recommendation


class SchoolForm(forms.ModelForm):
    class Meta:
        model = School
        fields = ['name', 'type', 'profile', 'city', 'street', 'street_no', 'county', 'email', 'phone_number', 'user']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Te rog sa introduci numele scolii', 'class': 'form-control'}),
            'type': Select(attrs={'class': 'form-control'}),
            'profile': TextInput(attrs={'placeholder': 'Te rog sa introduci profilul scolii', 'class': 'form-control'}), # cf models?
            'city': TextInput(attrs={'placeholder': 'Te rog să introduci orașul tău', 'class': 'form-control'}),
            'street': TextInput(attrs={'placeholder': 'Te rog sa introduci numele strazii liceului', 'class':'form-control'}),
            'street_no': TextInput(attrs={'placeholder': 'Te rog sa introduci numarul strazii liceului', 'class':'form-control'}),
            'county': TextInput(attrs={'placeholder': 'Te rog să introduci județul tău', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Te rog sa introduci adresa de email oficiala a liceului', 'class':'form-control'}),
            'phone_number': TextInput(attrs={'placeholder': 'Te rog sa introduci numarul de telefon al liceului', 'class':'form-control'}),
            'user': forms.HiddenInput(),
        }


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'date_of_birth', 'age', 'gender', 'school', 'school_year', 'skills', 'city',
                  'county', 'email']

        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Te rog sa iti introduci prenumele', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Te rog sa iti introduci numele de familie', 'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'age': TextInput(attrs={'placeholder': 'Te rog sa iti introduci varsta', 'class': 'form-control'}),
            'gender': Select(attrs={'class': 'form-control'}),
            'school': Select(attrs={'class': 'form-control'}),
            'school_year': TextInput(attrs={'placeholder': 'Te rog sa introduci in ce clasa/ an scolar esti', 'class': 'form-control'}),
            'skills': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'city': TextInput(attrs={'placeholder': 'Te rog să introduci orașul tău', 'class': 'form-control'}),
            'county': TextInput(attrs={'placeholder': 'Te rog să introduci județul tău', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Te rog sa introduci adresa de email', 'class': 'form-control'})
        }


class ActionForm(forms.ModelForm):
    class Meta:
        model = Action
        fields = ['name', 'organizer', 'description', 'city', 'county', 'image', 'email', 'phone_number', 'started_at',
                  'ended_at']

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Te rog sa introduci numele actiunii', 'class': 'form-control'}),
            'organizer': TextInput(attrs={'placeholder': 'Te rog sa introduci numele organizatorului',
                                          'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Adaugă descrierea evenimentului', 'class': 'form-control'}),
            'city': TextInput(attrs={'placeholder': 'Te rog să introduci orașul', 'class': 'form-control'}),
            'county': TextInput(attrs={'placeholder': 'Te rog să introduci județul', 'class': 'form-control'}),
            'image': forms.FileInput(attrs={'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Te rog sa introduci adresa de email', 'class': 'form-control'}),
            'phone_number': TextInput(attrs={'placeholder': 'Te rog sa introduci numarul de telefon',
                                             'class': 'form-control'}),
            'started_at': DateInput(format='%d-%m-%y',
                                    attrs={'class': 'myDateClass', 'placeholder': 'Selectați data de început',
                                           'type': 'date'}),
            'ended_at': DateInput(format='%d-%m-%y',
                                    attrs={'class': 'myDateClass', 'placeholder': 'Selectați data de sfarsit',
                                           'type': 'date'}),
        }


class SportForm(forms.ModelForm):
    class Meta:
        model = Sport
        fields = ['name', 'organizer', 'description', 'city', 'county', 'image', 'email', 'phone_number', 'started_at','ended_at', 'taxes', 'rewards']

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Te rog sa introduci numele actiunii', 'class': 'form-control'}),
            'organizer': TextInput(attrs={'placeholder': 'Te rog sa introduci numele organizatorului', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Adaugă descrierea evenimentului', 'class': 'form-control'}),
            'city': TextInput(attrs={'placeholder': 'Te rog să introduci orașul', 'class': 'form-control'}),
            'image': forms.FileInput(attrs={'class': 'form-control'}),
            'county': TextInput(attrs={'placeholder': 'Te rog să introduci județul', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Te rog sa introduci adresa de email', 'class': 'form-control'}),
            'phone_number': TextInput(attrs={'placeholder': 'Te rog sa introduci numarul de telefon', 'class': 'form-control'}),
            'started_at': DateInput(format='%d-%m-%y',
                                    attrs={'class': 'myDateClass', 'placeholder': 'Selectați data de început',
                                           'type': 'date'}),
            'ended_at': DateInput(format='%d-%m-%y',
                                    attrs={'class': 'myDateClass', 'placeholder': 'Selectați data de sfarsit',
                                           'type': 'date'}),
            'taxes': TextInput(attrs={'class': 'form-control'}),
            'rewards': TextInput(attrs={'class': 'form-control'}),
            }


class RecommendationForm(forms.ModelForm):
    class Meta:
        model = Recommendation
        fields = ['type', 'title', 'author', 'description']





