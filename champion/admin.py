from django.contrib import admin

from champion.models import *

admin.site.register(School)
admin.site.register(Skill)
admin.site.register(Student)
admin.site.register(Action)
admin.site.register(Sport)
admin.site.register(Recommendation)



