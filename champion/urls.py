from django.contrib import admin
from django.urls import path

from champion import views

urlpatterns = [
    path('', views.HomepageTemplateView.as_view(), name='homepage'),
    path('create_school/', views.SchoolCreateView.as_view(), name='create-school'),
    path('create_student/', views.StudentCreateView.as_view(), name='create-student'),
    path('create_action/', views.ActionCreateView.as_view(), name='create-action'),
    path('create_sport/', views.SportCreateView.as_view(), name='create-sport'),
    path('create-recommendation/', views.RecommendationCreateView.as_view(), name='create-recommendation'),
    path('list_schools/', views.SchoolListView.as_view(), name='list-schools'),
    path('list_actions/', views.ActionListView.as_view(), name='list-actions'),
    path('list_sports/', views.SportListView.as_view(), name='list-sports'),
    path('list_recommendations/', views.RecommendationListView.as_view(), name='list-recommendations'),
    path('update_school/<int:pk>', views.SchoolUpdateView.as_view(), name='update-school'),
    path('update_student/<int:pk>', views.StudentUpdateView.as_view(), name='update-student'),
    path('update_action/<int:pk>', views.ActionUpdateView.as_view(), name='update-action'),
    path('update_sport/<int:pk>', views.SportUpdateView.as_view(), name='update-sport'),
    path('delete_school/<int:pk>', views.SchoolDeleteView.as_view(), name='delete-school'),
    path('delete_student/<int:pk>', views.StudentDeleteView.as_view(), name='delete-student'),
    path('delete_action/<int:pk>', views.ActionDeleteView.as_view(), name='delete-action'),
    path('delete_sport/<int:pk>', views.SportDeleteView.as_view(), name='delete-sport'),
    path('detail_school/<int:pk>', views.SchoolDetailView.as_view(), name='detail-school'),
    path('detail_action/<int:pk>', views.action_detail_view, name='detail-action'),
    path('detail_sport/<int:pk>', views.SportDetailView.as_view(), name='detail-sport'),
]
