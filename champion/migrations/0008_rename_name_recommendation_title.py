# Generated by Django 3.2.9 on 2022-03-09 22:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('champion', '0007_recommendation_author'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recommendation',
            old_name='name',
            new_name='title',
        ),
    ]
