import django_filters

from django.db import models
from champion.models import School, Action, Sport, Recommendation


class SchoolFilter(django_filters.FilterSet):
    class Meta:
        model = School
        fields = ['name', 'city', 'county']


class ActionFilter(django_filters.FilterSet):
    class Meta:
        model = Action
        fields = ['city', 'county']


class SportFilter(django_filters.FilterSet):
    class Meta:
        model = Sport
        fields = ['city', 'county']


class RecommendationFilter(django_filters.FilterSet):
    class Meta:
        model = Recommendation
        fields = ['type']