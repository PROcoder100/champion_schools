from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, TemplateView, DeleteView

from champion.filters import SchoolFilter, ActionFilter, SportFilter, RecommendationFilter
from champion.models import School, Student, Action, Sport, ActionUser, Recommendation, SportUser
from champion.forms import SchoolForm, StudentForm, ActionForm, SportForm, RecommendationForm


class HomepageTemplateView(TemplateView):
    template_name = 'home/homepage.html'


class SchoolCreateView(CreateView):
    template_name = 'champion_schools/create_school.html'
    model = School
    success_url = reverse_lazy('create-school')
    form_class = SchoolForm

    def get_initial(self):
        return {
            'user': self.request.user.id
        }


class StudentCreateView(CreateView):
    template_name = 'champion_schools/create_student.html'
    model = Student
    success_url = reverse_lazy('create-student')
    form_class = StudentForm


class ActionCreateView(CreateView):
    template_name = 'champion_schools/create_action.html'
    model = Action
    success_url = reverse_lazy('create-action')
    form_class = ActionForm


class SportCreateView(CreateView):
    template_name = 'champion_schools/create_sport.html'
    model = Sport
    success_url = reverse_lazy('create-sport')
    form_class = SportForm


class RecommendationCreateView(CreateView):
    template_name = 'champion_schools/create-recommendation.html'
    model = Recommendation
    success_url = reverse_lazy('create-recommendation')
    form_class = RecommendationForm


class SchoolListView(ListView):
    template_name = 'champion_schools/list_schools.html'
    model = School
    context_object_name = 'all_schools'

    def get_context_data(self, *args, **kwargs):
        data = super(SchoolListView, self).get_context_data(**kwargs)
        all_schools = School.objects.all()
        my_filter = SchoolFilter(self.request.GET, queryset=all_schools)
        all_schools = my_filter.qs
        data['all_schools'] = all_schools
        data['my_filter'] = my_filter
        return data


class ActionListView(ListView):
    template_name = 'champion_schools/list_actions.html'
    model = Action
    context_object_name = 'all_actions'

    def get_context_data(self, *args, **kwargs):
        data = super(ActionListView, self).get_context_data(**kwargs)
        all_actions = Action.objects.all()
        my_filter = ActionFilter(self.request.GET, queryset=all_actions)
        all_actions = my_filter.qs
        data['all_actions'] = all_actions
        data['my_filter'] = my_filter
        return data


class SportListView(ListView):
    template_name = 'champion_schools/list_sports.html'
    model = Action
    context_object_name = 'all_sports'

    def get_context_data(self, *args, **kwargs):
        data = super(SportListView, self).get_context_data(**kwargs)
        all_sports = Sport.objects.all()
        my_filter = SportFilter(self.request.GET, queryset=all_sports)
        all_sports = my_filter.qs
        data['all_sports'] = all_sports
        data['my_filter'] = my_filter
        return data


class RecommendationListView(ListView):
    template_name = 'champion_schools/list_recommendation.html'
    model = Recommendation
    context_object_name = 'all_recommendation'

    def get_context_data(self, *args, **kwargs):
        data = super(RecommendationListView, self).get_context_data(**kwargs)
        all_recommendations = Recommendation.objects.all()
        my_filter = RecommendationFilter(self.request.GET, queryset=all_recommendations)
        all_recommendations = my_filter.qs
        data['all_recommendations'] = all_recommendations
        data['my_filter'] = my_filter
        return data


class SchoolDetailView(DetailView):
    template_name = 'champion_schools/detail_school.html'
    model = School


class ActionDetailView(DetailView):
    template_name = 'champion_schools/detail_action.html'
    model = Action


def action_detail_view(request, pk):
    action = Action.objects.get(id=pk)
    user = request.user
    if request.method == 'POST':
        ActionUser.objects.create(action=action, user=user)
    if user.is_authenticated:
        user_subscribed = ActionUser.objects.filter(action=action, user=user).exists()
    else:
        user_subscribed = []
    return render(request, 'champion_schools/detail_action.html',
                  {'action': action, 'user_subscribed': user_subscribed})


class SportDetailView(DetailView):
    template_name = 'champion_schools/detail_sport.html'
    model = Sport

    def sport_detail_view(request, pk):
        action = Sport.objects.get(id=pk)
        user = request.user
        if request.method == 'POST':
            SportUser.objects.create(sport=sport, user=user)
        if user.is_authenticated:
            user_subscribed = SportUser.objects.filter(sport=sport, user=user).exists()
        else:
            user_subscribed = []
        return render(request, 'champion_schools/detail_sport.html',
                      {'action': action, 'user_subscribed': user_subscribed})


class SchoolUpdateView(UserPassesTestMixin, UpdateView):
    template_name = 'champion_schools/update_school.html'
    model = School

    def test_func(self):
        if self.request.user == self.object.user:
            return True
        else:
            return False


class StudentUpdateView(UpdateView):
    template_name = 'champion_schools/update_student.html'
    model = Student


class ActionUpdateView(UpdateView):
    template_name = 'champion_schools/update_action.html'
    model = Action


class SportUpdateView(UpdateView):
    template_name = 'champion_schools/update_sport.html'
    model = Sport


class SchoolDeleteView(DeleteView):
    template_name = 'champion_schools/delete_school.html'
    model = School


class StudentDeleteView(DeleteView):
    template_name = 'champion_schools/delete_student.html'
    model = Student


class ActionDeleteView(DeleteView):
    template_name = 'champion_schools/delete_action.html'
    model = Action


class SportDeleteView(DeleteView):
    template_name = 'champion_schools/delete_sport.html'
    model = Sport
