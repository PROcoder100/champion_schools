from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class School(models.Model):
    TYPE_CHOICES = (
        ('Primary school', 'Școală primară'),
        ('Secondary school', 'Școală gimnazială'),
        ('Vocational school', 'Școală profesională'),
        ('High school', 'Liceu'),
        ('Junior college', 'Școală postliceală'),
        ('University', 'Universitate')
    )
    name = models.CharField(max_length=100, null=True, blank=True)
    type = models.CharField(max_length=100, choices=TYPE_CHOICES)
    profile = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    city = models.CharField(max_length=40, null=True, blank=True)  # excel cu lista localit din RO cu validare judet
    street = models.CharField(max_length=40, null=True, blank=True)
    street_no = models.CharField(max_length=40, null=True, blank=True)
    county = models.CharField(max_length=40, null=True, blank=True)
    email = models.EmailField()
    phone_number = models.CharField(max_length=30, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'


class Skill(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class Student(models.Model):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female')
    )

    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    date_of_birth = models.DateField()
    age = models.IntegerField()
    skills = models.ManyToManyField(Skill)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    school = models.ForeignKey(School, on_delete=models.SET_NULL, null=True, blank=True)
    school_year = models.IntegerField()  # clasa
    city = models.CharField(max_length=40, null=True, blank=True)  # excel cu lista localit din RO cu validare judet
    county = models.CharField(max_length=40, null=True, blank=True)  # excel cu lista judetelor din RO
    email = models.EmailField()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Action(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    organizer = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    city = models.CharField(max_length=40, null=True, blank=True)
    county = models.CharField(max_length=40, null=True, blank=True)
    image = models.ImageField(upload_to='static/actions')
    email = models.EmailField()
    phone_number = models.CharField(max_length=30)
    started_at = models.DateTimeField()
    ended_at = models.DateTimeField()

    def __str__(self):
        return f'{self.name} {self.organizer}'


class ActionUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    action = models.ForeignKey(Action, on_delete=models.CASCADE)


class Sport(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    organizer = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    city = models.CharField(max_length=40, null=True, blank=True)
    county = models.CharField(max_length=40, null=True, blank=True)
    image = models.ImageField(upload_to='static/sports')
    email = models.EmailField()
    phone_number = models.CharField(max_length=30)
    started_at = models.DateTimeField()
    ended_at = models.DateTimeField()
    taxes = models.TextField(max_length=100, null=True, blank=True)
    rewards = models.TextField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f'{self.name} {self.organizer}'


class SportUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sport = models.ForeignKey(Action, on_delete=models.CASCADE)


class Recommendation(models.Model):
    RECOMMENDATION_CHOICES = (
        ('Book', 'Book'),
        ('Movie', 'Movie'),
        ('Documentary', 'Documentary')
    )
    title = models.CharField(max_length=100, null=True, blank=True)
    type = models.CharField(max_length=100, null=True, blank=True, choices=RECOMMENDATION_CHOICES)
    author = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.title} {self.type}'